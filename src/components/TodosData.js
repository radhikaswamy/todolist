const todosData =[
    {
        id: 1,
        text: "Take out the trash",
        completed: false
    },

    {
        id: 2,
        text: "Grocery shopping",
        completed: false
    },

    {
        id: 3,
        text: "clean tank",
        completed: false
    },

    {
        id: 4,
        text: "Catch up an arrested Development",
        completed: true
    },
    {
        id: 5,
        text: "make a document",
        completed: false
    }
]

export default todosData