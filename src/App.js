import React from 'react';
import logo from './logo.svg';
import './App.css';
import TodoItems from './components/TodoItems'
import todosData from './components/TodosData'


class App extends React.Component{

  constructor(){
    super()
    this.state={
       todos: todosData
    }

    this.handleChange =this.handleChange.bind(this)  
  }

  handleChange(id){
    //console.log("changed!", id)

    this.setState (prevState =>{
      const updatedTodos =  prevState.todos.map(todo =>{

        // this code will directly modify the state of original object 
        // if (todo.id === id){
        //   todo.completed =! todo.completed
        // } return todo

      
        if (todo.id === id){
          return{
            ...todo,   // ... notation will give all the properties.
            completed: !todo.completed
          }
        } return todo
      })

      // console.log(prevState.todos)
      // console.log(updatedTodos)

      return {
        todos: updatedTodos
      }
    })
  }

  render(){
    const todoItems= this.state.todos.map(item => <TodoItems   key ={item.id} item={item} 
    handleChange={this.handleChange}/>)

    return (
      
      <div className="todo-list">
        {todoItems}
      </div>
    )
    }
  }
    

export default App;
